class UrlMappings {

	static mappings =
	{
		"/$controller/$action?/$id?"
		{
			constraints {}
		}
 		"/search/autocourse" (controller: "search")
		{
			action = [ POST: "autocourse"]
		}
		"/search/coldfusion" (controller: "search")
		{
			action = [ POST: "coldfusion"]
		}
		"/request/selected" (controller: "request")
		{
			action = [ POST: "selected"]
		}
		"/"(view:"/index")
		"500"(view:'/error')
	}
}
