package etrainu.autocourse

import groovy.sql.*

class LegacyDatabaseService
{
	static Sql sql
	
    Sql Connect (Boolean clear_cache)
	{
		if (!sql || clear_cache) sql = Sql.newInstance (
			"jdbc:sqlserver://localhost:1433;databaseName=etrainu",
			"sa", "Etrainu1",
			"com.microsoft.sqlserver.jdbc.SQLServerDriver")
		
		return sql
	}
}
