package etrainu.autocourse

import grails.converters.*
import groovy.xml.MarkupBuilder

class RequestController
{
	def LegacyDatabaseService
	
    def selected =
	{
		def search_text = request.reader.text;
		def search_xml = new XmlSlurper().parseText (search_text)

		if (search_xml.db == "cf") // coldfusion request
		{
			String guid = search_xml.guid
			def sql = LegacyDatabaseService.Connect (false);
			def induction = sql.firstRow ("select * from tbl_induction where inductionID = '" + guid + "'")
			def creator = sql.firstRow ("select * from tbl_user where userID = '" + induction.createdBy + "'")
			String create_user = creator.userName;
			String create_date = (induction.archiveDate ?: "0000-00-00");
			if (create_date.size() > 9) create_date = create_date[0..9];
			List wddx_types = ["unknown", "text", "video", "sound", "document", "flash", "image"];
			def writer = new StringWriter()
			def xml = new MarkupBuilder (writer)
			xml.course (){
				xml.info(){
					xml.title (induction.inductionName)
					xml.client ("etrainu")
					xml.code (induction.courseCode)
					xml.guid (induction.inductionID)
					xml.modified (user:create_user, date:create_date){}
					xml.version (schema:1, revision:1, "From legacy database")}
				sql.eachRow ("select * from tbl_stageInInduction a inner join tbl_stage b on a.fk_stageID = b.stageID where fk_inductionID = '" + guid + "' order by orderInInduction asc"){
					def stage_id = it.stageID;
					xml.stage (name:it.stageName, ord:it.orderInInduction){
						sql.eachRow ("select * from tbl_slideInStage a inner join tbl_slide b on a.fk_slideID = b.slideID where fk_stageID = '" + stage_id + "' order by orderInStage asc"){
							def slide_id = it.slideID;
							xml.slide (name:it.slideName, ord:it.orderInStage){
								// slide elements
								sql.eachRow ("select * from tbl_slideElement where fk_slideID = '" + slide_id + "' order by orderInSlide"){
									def element_text = it.wddxData;
									def element_type = wddx_types[it.fk_slideElementTypeID];
									xml.wddx (type:element_type, element_text);}}
							// assessments
							sql.eachRow ("select * from tbl_assessmentInSlide a inner join tbl_assessment b on a.fk_assessmentID = b.assessmentID where fk_slideID = '" + slide_id + "'"){
								def assessment_id = it.assessmentID;
								xml.assessment (name:it.assessmentName, pass:it.requiredPassPercentage){
									sql.eachRow ("select * from tbl_question where fk_assessmentID = '" + assessment_id + "' order by orderInAssessment"){
										def question_id = it.questionID;
										def question_text = it.question;
										xml.question (ord:it.orderInAssessment, type:'choice'){
											xml.text (question_text)
											sql.eachRow ("select * from tbl_answer where fk_questionID = '" + question_id + "' order by orderInQuestion"){
												def is_answer = (Boolean)(it.answerIsCorrect);
												def answer_text = it.answer;
												xml.option (ord:it.orderInQuestion, answer:is_answer.toString()){
													xml.text (answer_text);}}}}}}}}}}
			render writer.toString()
		}
	}
}
