package etrainu.autocourse

import groovy.sql.*

class StatusController
{
	def LegacyDatabaseService

    def index =
	{
		Integer course_count = 0; //Course.count();
		
		// Connect to legacy database
		Sql sql = LegacyDatabaseService.Connect (true);
		Integer legacy_course_count = sql ? sql.firstRow ("select count(*) as count from tbl_induction").count : 0

		return [
			course_count : course_count,
			legacy_status : sql ? "connected" : "not connected",
			legacy_course_count : legacy_course_count ]
	}
}
