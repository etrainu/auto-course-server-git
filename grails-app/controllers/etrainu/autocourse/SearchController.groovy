package etrainu.autocourse

import grails.converters.*

class SearchController
{
	def LegacyDatabaseService

    def autocourse =
	{
		def results = []
		render results as XML
	}

    def coldfusion =
	{
		def search_text = request.reader.text;
		def search_xml = new XmlSlurper().parseText (search_text)
		def results = []
		def sql = LegacyDatabaseService.Connect (false);
		def query = "select top 101 "
		query += "inductionID, inductionName, courseCode "
		query += "from tbl_induction where ";

		Integer term_count = 0;
		search_xml.term.each {
			def word = it.text()
			if (term_count++) query += " and "
			query += "(lower (inductionID) like '%" + word + "%' or "
			query += "lower (inductionName) like '%" + word + "%' or "
			query += "lower (courseCode) like '%" + word + "%')"}

		sql.eachRow (query, { results << [db:"cf", guid:it.inductionID, title:it.inductionName, code:it.courseCode]})
		render results as XML
	}
}
