<html>
    <head>
        <title>AutoCourse Server</title>
        <style type="text/css" media="screen">

        h1 {
            margin-top:10px;
            margin-bottom:10px;
            font-weight:normal;
            font-size:22px;
            color:#909090
        }
        h2 {
            margin-top:30px;
            margin-bottom:10px;
            font-weight:normal;
            font-size:16px;
            color:#606060
        }
        p {
            font-size:12px;
		}        
        </style>
    </head>
    <body>
        <div>
            <h1>AutoCourse Server Version 0.1</h1>
			<p>Course count = ${course_count}</p>

			<h2>Legacy Database</h2>
			<p>Status = ${legacy_status}</p>
			<p>Course count = ${legacy_course_count}</p>
        </div>
    </body>
</html>
